#!/bin/bash

version="0.0.5"

#stop script if a command fails
set -e

if ! command -v lsb_release &> /dev/null
then
    echo "Installing ubuntu-dev-tools"
    sudo apt install ubuntu-dev-tools -y
fi

arch=`dpkg --print-architecture`
release=`cat /etc/lsb-release | grep DISTRIB_RELEASE | sed "s/.*=//g"`

## deps to backport, listed by source package name
if [ $release = "18.04" ]; then
    deps="freetype dvisvgm requests sphinx pydata-sphinx-theme cython python-hypothesis numpy numpydoc scipy googletest cpuinfo fxdiv pthreadpool psimd fp16 xnnpack libnop meson simde python-typing-extensions sleef fmtlib liblo curl"
    #libhttp-tiny-multipart-perl libgitlab-api-v4-perl devscripts 
else
    deps="fxdiv cpuinfo pthreadpool psimd fp16 cpuinfo xnnpack libnop meson simde cython numpy boost1.74 boost-defaults"
    ## deps available in jammy, but outdated for pytorch master: "gloo onednn onnx tensorpipe", in dependency building order:
    # deps="fxdiv pthreadpool psimd fp16 cpuinfo xnnpack gloo libnop tensorpipe onnx meson simde onednn ideep"
fi

echo "Backporting $deps"
for d in $deps; do
    upstream="jammy"
    if [ $release = "18.04" ]; then
        if [ "$d" = "libhttp-tiny-multipart-perl" ] || [ "$d" = "libgitlab-api-v4-perl" ] || [ "$d" = "devscripts" ] || [ "$d" = "sleef" ] || [ "$d" = "fmtlib" ] || [ "$d" = "curl" ]; then
            upstream="focal"
        fi
        if [ "$d" = "python-hypothesis" ] || [ "$d" = "sphinx" ] || [ "$d" = "numpy" ] || [ "$d" = "numpydoc" ] || [ "$d" = "scipy" ] || [ "$d" = "dvisvgm" ] || [ "$d" = "simde" ]; then
            upstream="hirsute"
        fi
    fi
    echo "Backporting $d from $upstream"
    if ! $(ls -d $d-* 1> /dev/null 2>&1); then
        pull-lp-source $d $upstream
    fi

    l=`echo $d | sed "s/lib//g"`
    if [ "$d" = "boost-defaults" ]; then
        l="libboost-dev"
    fi
    if $(ls *$l*.deb 1> /dev/null 2>&1); then
        echo "deb packages already found for $d, skipping packaging"
        continue
    fi

    pushd $d-*;
    sed -i "s/debhelper[-compat]* ([>]*= [0-9]*[~]*)/debhelper-compat (= 11)/g" debian/control;
    if [ "$d" = "requests" ]; then
        # to fix error: dh: Compatibility levels before 5 are no longer supported (level 1 requested)
        echo 11 > debian/compat
    else
        touch debian/compat && rm debian/compat
    fi

    if [ $release = "18.04" ]; then
        sed -i "s/dh-sequence-python3/dh-python/g" debian/control;
    fi
    if [ $release = "18.04" ] && [ "$d" = "xnnpack" ]; then
        sed -i "s/cmake+ninja/cmake/g" debian/rules;
    fi
    if [ $release = "18.04" ] && [ "$d" = "simde" ]; then
        sed -i "s/meson+ninja/meson/g" debian/rules;
    fi
    if [ "$d" = "fxdiv" ]; then
        sed -i "s/-DFXDIV_BUILD_TESTS=ON -DFXDIV_BUILD_BENCHMARKS=ON/-DFXDIV_BUILD_TESTS=OFF -DFXDIV_BUILD_BENCHMARKS=OFF/g" debian/rules;
    fi
    if [ "$d" = "fmtlib" ]; then
        sed -i "s/-B bdir//g" debian/rules;
        sed -i "s/dh_auto_configure -- /dh_auto_configure -- -DFMT_TEST=OFF /g" debian/rules;
        sed -i "s/dh_auto_build  -- doc/#dh_auto_build  -- doc/g" debian/rules;
        sed -i "s/dpkg-shlibdeps/#dpkg-shlibdeps/g" debian/rules;
        echo > debian/libfmt-doc.install
    fi
    if [ "$d" = "scipy" ]; then
        sed -i "s/const //g" scipy/*/*/*.pyx;
        sed -i "s/const //g" scipy/*/*.pyx;
        sed -i 's/if sys.version_info\[:2\] < (3, 7)://g' setup.py
        sed -i 's/    raise RuntimeError("Python version >= 3.7 required.")//g' setup.py
        echo "override_dh_installdocs:" >> debian/rules
    fi

    profiles=""
    if [ "$d" = "libgitlab-api-v4-perl" ] || [ "$d" = "meson" ]; then
        profiles="-Pnocheck"
    fi
    if [ "$d" = "python-hypothesis" ] || [ "$d" = "numpy" ]; then
        profiles="-Pnodoc,nocheck"
    fi
    echo "Using profile(s): $profiles"

    export DEB_BUILD_OPTIONS="parallel=`nproc` nocheck"
    if [ "$d" = "boost"* ]; then
        export DEB_BUILD_OPTIONS="parallel=`nproc` nodoc"
    fi

    mk_build_options=""
    if [ ! $release = "18.04" ]; then
        mk_build_options=$profiles
    fi
    echo "mk_build_options: ${mk_build_options}"
    sudo mk-build-deps ${mk_build_options} --install -t 'apt-get --force-yes -y' || \
    sudo ldconfig && \
    dpkg-checkbuilddeps $profiles
    flags=""
    if [ "$d" = "boost"* ]; then
        pyv=`python3 -V | awk -F ' ' '{print $2}' | awk -F '.' '{printf $1 "." $2}'`
        flags="CPLUS_INCLUDE_PATH=/usr/include/python$pyv"
    fi
    eval $flags dpkg-buildpackage $profiles -b -us -uc

    if [ "$d" = "freetype" ]; then
        rm ../libfreetype6-dev*.deb
    fi
    if [ "$d" = "fmtlib" ]; then
        d="libfmt"
    fi
    if [ "$d" = "boost"* ]; then
        d="boost"
    fi
    if [ "$d" = "googletest" ]; then
        sudo dpkg -i ../googletest*.deb ../libgtest-dev*.deb ../libgmock-dev*.deb
    else
        sudo dpkg -i ../*${d/-/*}*.deb
    fi
    sudo apt --fix-broken install -y
    rm *build-deps* -f
    for b in `apt list --installed | grep build-dep | awk -F '/' '{print $1}'`; do sudo apt remove $b -y; done
    popd
done
tar -cf pytorch-third-party-backports-${version}-${arch}-ubuntu${release}.tar.gz *.deb
