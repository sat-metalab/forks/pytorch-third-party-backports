# pytorch-third-party-backports

Backported apt packages of pytorch third party dependencies

pytorch has been packaged for ubuntu since hirsute: 
https://packages.ubuntu.com/source/hirsute/pytorch

This script backports some of pytorch's third party dependencies, for now for ubuntu focal, currently: 
fxdiv pthreadpool psimd fp16 cpuinfo xnnpack libnop meson simde ideep

## Instructions

Run: `./backport-packages.sh`

This will create an archive containing backported apt deb packages, for the distribution in which the script has been ran. 
For example for ubuntu focal on x86_64 architectures: 
[pytorch-third-party-backports-amd64-ubuntu20.04.tar.gz](pytorch-third-party-backports-amd64-ubuntu20.04.tar.gz)
